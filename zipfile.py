# -*- coding: utf-8 -*-
'''
Python-native, platform independent zip extraction salt state
Version: 1
Author: Ruben Orduz
License: Apache v2
'''

import zipfile
import os, logging

log = logging.getLogger(__name__)

def extracted(name, source=None, destination=None):
    ret = {'name': name, 'result': None, 'changes': {}, 'comment': ''}
    '''
    In order to avoid having to download or install or manage unzipping utilities, this
    state will unzip files w/o needing those, platform independent.

    Example:

    my_state_name:
        zipfile.extracted:
            - source: /this/that.zip
            - destination: /that/other/path

    params:
        source: the fully qualified path in the file system where the file can be found
        destination: the fully qualified path in the file system where you wish the contents
        of the zip file should be extracted to
    '''
    if not source:
        log.error('No source file provided.')
        ret['comment'] = 'No source file provided.'
        ret['result'] = False
        return ret
    if not destination:
        log.error('No destination provided')
        ret['comment'] = 'No destination provided'
        ret['result'] = False
        return ret
    if not zipfile.is_zipfile(os.path.abspath(source)):
        log.error('Specified file is not a valid zip format.')
        ret['comment'] = 'Specified file is not a valid zip format.'
        ret['result'] = False
        return ret
    try:
        zf = zipfile.ZipFile(os.path.abspath(source))
    except Exception as e:
        log.error('Could not open file: ' + str(e))
        ret['comment'] = 'Could not open file: ' + str(e)
        ret['result'] = False
        return ret
    try:
        zf.extractall(os.path.abspath(destination))
        log.info('file successfully extracted to destination.')
        ret['comment'] = 'Files extracted to: ' + destination
        ret['changes'] = 'Files extracted to: ' + destination
        ret['result'] = True
        return ret
    except Exception as e:
        log.error('Could not extract file: ' + str(e))
        ret['comment'] = 'Could not extract file: ' + str(e)
        ret['result'] = False
        return ret


